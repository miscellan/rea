'use strict';

angular.module('propFinder.base', [
    'ui.router'
])

    .config(function($stateProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: 'app/base/base.html'
            });
    });
