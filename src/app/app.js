'use strict';

angular.module('propFinder', [
        'ui.router',
        'propFinder.base',
        'propFinder.search'
    ])

    .config(function($httpProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    });
