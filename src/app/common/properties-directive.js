'use strict';

angular.module('propFinder.properties-directive', [])

    .directive('pfProperties', function() {
        return {
            restrict: 'A',
            scope: {
                properties: '=pfProperties'
            },
            templateUrl: 'app/common/properties-directive.html'
        };
    });
