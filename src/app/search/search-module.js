'use strict';

angular.module('propFinder.search', [
    'ui.router',
    'propFinder.api.resource',
    'propFinder.search-ctrl'
])

    .config(function($stateProvider) {

        $stateProvider.state('app.search', {
            url: '/',
            resolve: {
                props: function(propResults) {
                    return propResults.get();
                }
            },
            views: {
                content: {
                    controller: 'SearchCtrl',
                    controllerAs: 'ctrl',
                    templateUrl: 'app/search/search.html'
                }
            }
        });
    });
