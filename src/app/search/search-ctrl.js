'use strict';

angular.module('propFinder.search-ctrl', [
    'propFinder.properties-directive',
    'propFinder.api.savedProperties'
])

    .controller('SearchCtrl', function($scope, props, propSavedProperties) {
        $scope.props = angular.copy(props);

        // isSaved as a property on the object is purely for the UI.
        var findSaved = function() {
            _.forEach($scope.props.results, function(prop) {
                prop.isSaved = !!_.find($scope.props.saved, ['id', prop.id]);
            });
        };

        // not strictly necessary to call this on initialise for the provided data, but required without strictly knowing what's in the results.
        findSaved();

        $scope.updateSaved = function(prop, add) {
            if (add) {
                // don't want to add the same thing twice
                if (_.find(props.saved, ['id', prop.id])) {
                    return;
                }
                props.saved.push(prop);
            } else {
                props.saved = _.reject(props.saved, ['id', prop.id]);
            }
            propSavedProperties.save(props.saved).then(function() {
                $scope.props = angular.copy(props);
                findSaved();
            });
        };
    });