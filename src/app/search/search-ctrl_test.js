'use strict';

describe('app.search module', function() {
    var $scope, props, propSavedProperties;

    beforeEach(function() {
        module('propFinder.search');

        propSavedProperties = jasmine.createSpyObj('propSavedProperties', ['save']);
        propSavedProperties.save.and.returnValue({
            then: function(cb) {
                return cb();
            }
        });

        props = {
            "results": [{
                "price": "$726,500",
                "id": "1"
            }, {
                "price": "$560,520",
                "id": "2"
            }, {
                "price": "$826,500",
                "id": "3"
            }],
            "saved": [{
                "price": "$726,500",
                "id": "1"
            }, {
                "id": "4",
                "price": "$526,500"
            }]
        };


        $scope = {};
        inject(function($controller) {
            $controller('SearchCtrl', {
                $scope: $scope,
                props: props,
                propSavedProperties: propSavedProperties
            });
        });
    });

    it('updateSaved additions works as expected', function() {
        var preSaveProps = angular.copy(props);
        expect($scope.props.results.length).toEqual(3);
        expect($scope.props.saved.length).toEqual(2);

        // tests simple save
        $scope.updateSaved(_.find(props.results, ['id', '2']), true);
        expect($scope.props.saved.length).toEqual(3);
        expect(_.find($scope.props.saved, ['id', '2']).price).toEqual('$560,520');

        expect(propSavedProperties.save).toHaveBeenCalledWith(props.saved);
        expect(preSaveProps.saved.length).toEqual(2);
        expect(props.saved.length).toEqual(3);
        propSavedProperties.save.calls.reset();

        // tests no duplication of simple save
        $scope.updateSaved(_.find(props.results, ['id', '2']), true);
        expect($scope.props.saved.length).toEqual(3);

        expect(propSavedProperties.save).not.toHaveBeenCalled();

        // tests nothing removed from original results
        expect(props.results.length).toEqual(3);
    });

    it('updateSaved remove works as expected', function() {
        var preSaveProps = angular.copy(props);

        expect($scope.props.results.length).toEqual(3);
        expect($scope.props.saved.length).toEqual(2);

        // tests removal
        $scope.updateSaved({id: '4'}, false);
        expect($scope.props.saved.length).toEqual(1);

        expect(propSavedProperties.save).toHaveBeenCalledWith(props.saved);
        expect(preSaveProps.saved.length).toEqual(2);
        expect(props.saved.length).toEqual(1);
        propSavedProperties.save.calls.reset();

        // tests removal of object that's not there doesn't blow anything up
        $scope.updateSaved({id: '4'}, false);
        expect($scope.props.saved.length).toEqual(1);

        expect(propSavedProperties.save).toHaveBeenCalledWith(props.saved);

        // tests nothing removed from original results
        expect(props.results.length).toEqual(3);
    });

    it('updateSaved sets isSaved appropriately', function() {
        var results = _.filter($scope.props.results, 'isSaved');
        expect(results.length).toEqual(1);
        expect(results[0].id).toEqual('1');
        expect(_.filter($scope.props.saved, 'isSaved')).toEqual([]);

        // tests adding an element produces isSaved appropriately
        $scope.updateSaved(_.find(props.results, ['id', '2']), true);
        expect(_.find($scope.props.saved, ['id', '2']).isSaved).toEqual(true);
        expect(_.find($scope.props.results, ['id', '2']).isSaved).toEqual(true);

        // tests removal of object also puts isSaved into false
        $scope.updateSaved(_.find(props.results, ['id', '2']), false);
        expect(_.find($scope.props.saved, ['id', '2'])).toEqual(undefined);
        expect(_.find($scope.props.results, ['id', '2']).isSaved).toEqual(false);
    });
});
