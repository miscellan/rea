'use strict';

angular.module('propFinder.api.savedProperties', [])

    .factory('propSavedProperties', function(propResource, $q) {
        var propResults = propResource('savedProperties');

        propResults.save = function(savedResults) {
            // let's just pretend that a save to the API has happened here
            return $q.when(savedResults).then(function(apiResponseData) {
                return apiResponseData;
            });
        };

        return propResults;
    });
