'use strict';

angular.module('propFinder.api.resource', [
    'ngResource',
    'propFinder.api.results'
])

    .factory('propResource', function($resource) {
        return function(pathSettings) {
            var uri = 'http://pretendapiserver.com/' + pathSettings;
            return $resource(uri);
        };
    });
