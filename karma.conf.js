module.exports = function(config) {
    config.set({
        // do not set files here, these are included via gulp

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-chrome-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-ng-html2js-preprocessor'
        ],

        reporters: ['dots', 'junit'],

        junitReporter: {
            outputDir: 'reports',
            outputFile: 'test-karma-results.xml',
            suite: 'unit'
        },

        preprocessors: {
            'src/**/*.html': 'ng-html2js'
        },

        ngHtml2JsPreprocessor: {
            stripPrefix: 'src/',
            moduleName: 'cxad.test.templates'
        }
    });
};
