'use strict';

var gulp = require('gulp'),
    Server = require('karma').Server,
    bowerDeps = require('wiredep')({
        dependencies: true,
        devDependencies: true
    }),
    testFiles = bowerDeps.js.concat([
        'src/{app,components}/**/*.js',
        'src/{app,components}/**/*.html'
    ]);

gulp.task('test', function(done) {
    new Server({
        configFile: __dirname + '/../karma.conf.js',
        files: testFiles,
        singleRun: true
    }, function(exitStatus) {
        if (exitStatus) {
            console.warn('Karma exited with status', exitStatus);
        }
        done();
    }).start();
});

gulp.task('test:watch', function() {
    new Server({
        configFile: __dirname + '/../karma.conf.js',
        files: testFiles
    }).start();
});
