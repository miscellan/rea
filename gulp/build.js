'use strict';

var gulp = require('gulp'),
    del = require('del'),
    lazypipe = require('lazypipe'),
    wiredep = require('wiredep').stream,
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license']
    });

var exec = require('child_process').exec;

function exec_command(command) {
    return function(cb) {
        exec(command, function(err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            cb(err);
        });
    }
}

function handleError(err) {
    console.error(err.toString());
    this.emit('end');
}

gulp.task('styles', function() {
    return gulp.src('src/{app,components,styles}/**/*.less')
        .pipe($.less({
            paths: [
                'src/bower_components',
                'src/app',
                'src/components'
            ],
            relativeUrls: true,
            strictMath: true,
            strictUnits: true
        }))
        .on('error', handleError)
        .pipe($.autoprefixer({browsers: ['last 2 versions']}))
        .pipe(gulp.dest('.tmp'))
        .pipe($.size());
});

gulp.task('scripts', function() {
    return gulp.src(['src/{app,components}/**/*.js'])
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.size());
});

gulp.task('inject', function() {
    return gulp.src('src/*.html')
        .pipe(wiredep())
        .pipe($.inject(gulp.src(['src/components/**/*.js', '!src/components/**/*_test.js'], {read: false}), {
            starttag: '<!-- inject:components -->',
            relative: true
        }))
        .pipe($.inject(gulp.src(['src/app/**/*.js', '!src/app/**/*_test.js'], {read: false}), {
            starttag: '<!-- inject:app -->',
            relative: true
        }))
        .pipe(gulp.dest('.tmp'))
        .pipe($.size());
});

gulp.task('partials:clean', function(cb) {
    del('.tmp/{app,components}/**/*.tpl.js').then(function() {
        cb();
    });
});

gulp.task('partials', ['partials:clean'], function() {
    return gulp.src('src/{app,components}/**/*.html')
        .pipe($.htmlmin({collapseWhitespace: true}))
        .pipe($.ngHtml2js({
            moduleName: 'propFinder',
            declareModule: false
        }))
        .pipe($.rename({
            extname: '.tpl.js'
        }))
        .pipe(gulp.dest('.tmp'))
        .pipe($.size());
});

gulp.task('html', ['clean', 'styles', 'partials', 'inject'], function() {
    var uglify = $.uglify,
        rev = $.rev;

    var jsPipe = lazypipe().pipe($.ngAnnotate).pipe(uglify).pipe(rev),
        vendorJsPipe = lazypipe().pipe(uglify, {preserveComments: $.uglifySaveLicense}).pipe(rev),
        cssPipe = lazypipe().pipe($.cssnano, {autoprefixer: false, reduceIdents: false}).pipe(rev);

    return gulp.src('.tmp/*.html')
        .pipe($.replace(/(ng-app="\w+")/, '$1 ng-strict-di'))
        .pipe($.inject(gulp.src('.tmp/{app,components}/**/*.tpl.js', {read: false}), {
            starttag: '<!-- inject:partials -->',
            addRootSlash: false,
            addPrefix: '..'
        }))
        .pipe($.useref())
        .pipe($.if(['**/*.js', '!**/vendor.js'], jsPipe()))
        .pipe($.if('**/vendor.js', vendorJsPipe()))
        .pipe($.if('*.css', cssPipe()))
        .pipe($.revReplace())
        .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
        .pipe(gulp.dest('build'))
        .pipe($.size());
});

gulp.task('clean', function(cb) {
    del('build').then(function() {
        cb();
    });
});

gulp.task('build', ['html']);
