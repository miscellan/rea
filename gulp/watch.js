'use strict';

var gulp = require('gulp');

gulp.task('watch', ['styles', 'inject'], function() {
    gulp.watch('src/*.html', ['inject']);
    gulp.watch('src/{app,components,styles}/**/*.less', ['styles']);
    gulp.watch(['src/{app,components}/**/*.js'], ['scripts', 'inject']);
});
