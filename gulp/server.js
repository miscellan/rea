'use strict';

var gulp = require('gulp'),
    inject = require('gulp-inject'),
    browserSync = require('browser-sync').create('propFinder'),
    spa = require('browser-sync-spa');

function browserSyncInit(baseDir, files, open, cb) {
    open = typeof open === 'undefined' ? 'local' : open;
    browserSync.use(spa({
        selector: '[ng-app]'
    }));
    browserSync.init({
        files: files,
        server: {
            baseDir: baseDir
        },
        open: open,
        notify: false,
        ghostMode: false,
        logLevel: 'warn'
    }, cb || function() {});
}

gulp.task('serve', ['watch'], function() {
    browserSyncInit([
        '.tmp',
        'src'
    ], [
        '.tmp/*.html',
        '.tmp/styles/*.css',
        'src/{app,components}/**/*.html',
        'src/{app,components}/**/*.js',
        'src/{app,components}/**/*.{jpg,jpeg,png,gif}'
    ]);
});

