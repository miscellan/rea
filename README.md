# Property Finder

This guide assumes the following:

*   you have Node.js (and npm, its package manager) installed on your machine.
*   you have git installed on your machine and have cloned the project to your local machine

## Running Up the Property Finder

If you haven't done so already on your machine, via npm, install gulp, which is a build management tool.
```bash
sudo npm install gulp -g
```

Once that's done, go to the project directory and use npm to install all the web resources and dependencies that are required like so:
```bash
cd <local git repo dir>
npm install
```

### Running Up the App Like a Developer

Now you can run up the app with:
```bash
npm start
```
You should now find your browser window opening at http://localhost:3000/ with the Property Finder app.

The app is being served with [BrowserSync](https://www.browsersync.io/), which automatically serves up
any changes you make while developing.

### Building the App for Release
To build the app for release, just do the following:
```bash
gulp build
```
This will build the app in the following directory:
```bash
<local git repo dir>/build
```
Load the file index.html from the build directory into your browser and you should find the built Property Finder running smoothly.

## Running Unit Tests Locally
To run the unit tests, do this, which does require Chrome to be installed on your machine:
```bash
gulp test
```

Alternatively, to have the tests run continuously while developing, do the following:
```bash
gulp test:watch
```
